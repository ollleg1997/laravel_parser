<?php

namespace Tests\Unit;

use App\Modules\Parser\src\ParseCategory;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class ParseCategoryTest extends TestCase
{
    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testFetching()
    {
        $content = file_get_contents(__DIR__.'/stubs/cat_1.html');
        Server::enqueue([new Response(200, [], $content)]);

        $parser = new ParseCategory(Server::$url, 'ul li a', '');
        $items = $parser->fetchItemList();

        $this->assertEquals($items, ['//link1', '//link2', '//link3', '//link4', '//link5']);
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testPaginate()
    {
        $cat_1 = file_get_contents(__DIR__.'/stubs/cat_1.html');
        $cat_2 = file_get_contents(__DIR__.'/stubs/cat_2.html');

        Server::enqueue([
            new Response(200, [], $cat_1),
            new Response(200, [], $cat_2),
        ]);

        $parser = new ParseCategory(Server::$url, 'ul li a', 'link[rel="next"]');
        $parser->nextPage();
        $items = $parser->fetchItemList();

        $this->assertEquals($items, ['//link6', '//link7', '//link8', '//link9', '//link10']);

        $url = $parser->nextPage();
        $this->assertEmpty($url);
    }
}
