<?php

namespace Tests\Unit;

use App\Modules\Parser\src\ParseItem;
use GuzzleHttp\Psr7\Response;
use Tests\TestCase;

class ParseItemTest extends TestCase
{
    /**
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function testParse()
    {
        $good = file_get_contents(__DIR__.'/stubs/good.html');
        Server::enqueue([new Response(200, [], $good)]);

        $parser = new ParseItem(Server::$url, [
            'title'       => 'h1',
            'description' => 'p',
        ]);

        $item = $parser->parse();

        $this->assertEquals($item, [
            'title'       => 'title',
            'description' => 'description',
        ]);
    }
}
