<?php

return [

    /*
     * Options for updating items
     */
    'update'     => [
        'max_items' => 10,
    ],

    /*
     * Options for iteration items
     */
    'objects'    => [
        'max_items' => 30,
    ],

    /*
     * Parent categories for parsing items
     */
    'categories' => [
        'category' => [
            'link'           => 'https://veliki.com.ua/dir_cross_country.htm',
            'category_class' => \App\Modules\Parser\BicycleCategoryParser::class,
            'item_class'     => \App\Modules\Parser\BicycleParser::class,
        ],
    ],

];
