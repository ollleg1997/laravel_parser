<?php

namespace App\Console\Commands;

use App\Modules\Parser\src\ConfigException;
use Illuminate\Console\Command;

class Parser extends Command
{
    /**
     * @var string
     */
    protected $signature = 'parser:run {--category= : Category alias name}';

    /**
     * @var string
     */
    protected $description = 'Start up pursing categories';

    /**
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \App\Modules\Parser\src\ConfigException
     */
    public function handle()
    {
        $configs = config('parser.categories');
        $categoryName = $this->option('category');

        if ($categoryName) {
            if (!array_key_exists($categoryName, $configs)) {
                throw new ConfigException('Not found category');
            }

            $this->parseCategory($categoryName);

            return;
        }

        foreach ($configs as $type => $config) {
            $this->parseCategory($type);
        }

        return;
    }

    public function parseCategory(string $type)
    {
        $parser = new \App\Modules\Parser\Parser($type);
        $parser->loadConfig();
        $parser->run();
    }
}
