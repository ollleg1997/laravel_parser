<?php
declare(strict_types = 1);

namespace App\Repositories;

use App\Bicycle as BicycleModel;

class Bicycle
{
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function isNew(): bool
    {
        return BicycleModel::where('code', (int)$this->data['code'])
                           ->doesntExist();
    }

    public function save(): int
    {
        return BicycleModel::create($this->data)->id;
    }

    public function update(): int
    {
        return (int)BicycleModel::where('code', $this->data['code'])
                                ->update($this->data);
    }
}
