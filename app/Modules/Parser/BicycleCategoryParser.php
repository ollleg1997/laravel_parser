<?php
declare(strict_types = 1);

namespace App\Modules\Parser;

use App\Modules\Parser\src\ParsebleCategory;
use App\Modules\Parser\src\ParseCategory;

class BicycleCategoryParser implements ParsebleCategory
{
    protected $pageSelector = 'link[rel="next"]';

    protected $itemSelector = 'ul li .holder .name a';

    protected $parser;

    public function __construct(string $link)
    {
        $this->parser = new ParseCategory($link, $this->itemSelector, $this->pageSelector);
    }

    /**
     * Get list of link to item page
     *
     * @return array
     */
    public function fetchItemList(): array
    {
        return $this->parser->fetchItemList();
    }

    /**
     * Get next page and set new context
     *
     * @return string
     * @throws \App\Modules\Parser\src\ConfigException
     */
    public function nextPage(): string
    {
        return $this->parser->nextPage();
    }
}
