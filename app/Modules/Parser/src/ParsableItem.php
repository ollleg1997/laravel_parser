<?php
declare(strict_types = 1);

namespace App\Modules\Parser\src;

interface ParsebleItem
{
    public function __construct(string $link);

    public function parse(): array;

    public function getIteratedCount(): int;
}
