<?php
declare(strict_types = 1);

namespace App\Modules\Parser\src;

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

abstract class BaseParse
{
    protected $link;

    /**
     * @var \Symfony\Component\DomCrawler\Crawler
     */
    protected $crawler;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    public function __construct(string $link)
    {
        $this->link = $link;
        $this->httpClient = new Client();

        $this->setCrawler();
    }

    /**
     *  Init crawler(html parser) with html context
     */
    protected function setCrawler(): void
    {
        $this->crawler = new Crawler(null, $this->link);
        $this->crawler->addHtmlContent($this->getContent(), 'UTF-8');
    }

    /**
     * Getting html context from site link
     *
     * @return string
     */
    protected function getContent(): string
    {
        $content = $this->httpClient->get($this->link)
                                    ->getBody()
                                    ->getContents();

        return mb_convert_encoding($content, "utf-8", "windows-1251");
    }
}
