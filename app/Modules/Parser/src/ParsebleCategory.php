<?php
declare(strict_types = 1);

namespace App\Modules\Parser\src;

interface ParsebleCategory
{
    public function __construct(string $link);

    /**
     * Get list of link to item page
     *
     * @return array
     */
    public function fetchItemList(): array;

    /**
     * Get next page and set new context
     *
     * @return string
     */
    public function nextPage(): string;
}
