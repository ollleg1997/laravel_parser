<?php
declare(strict_types = 1);

namespace App\Modules\Parser\src;

class ParseCategory extends BaseParse
{
    protected $itemLinks;

    protected $itemSelector;

    protected $pageSelector;

    /**
     * ParseCategory constructor.
     *
     * @param string $link         - Link to category page
     * @param string $itemSelector - selector to item link
     * @param string $pageSelector - selector ton next page link
     */
    public function __construct(string $link, string $itemSelector, string $pageSelector = '')
    {
        parent::__construct($link);

        $this->itemSelector = $itemSelector;
        $this->pageSelector = $pageSelector;
    }

    /**
     * @inheritdoc
     */
    public function fetchItemList(): array
    {
        $nodes = $this->crawler->filter($this->itemSelector);
        $this->itemLinks = [];

        foreach ($nodes as $node) {
            $this->itemLinks[] = $node->getAttribute('href');
        }

        return $this->itemLinks;
    }

    /**
     * @inheritdoc
     * @throws \App\Modules\Parser\src\ConfigException
     */
    public function nextPage(): string
    {
        if(empty($this->pageSelector)){
                throw new ConfigException('Page selector must be seated');
        }

        $node = $this->crawler->filter($this->pageSelector)
                              ->getNode(0);

        if (null === $node) {
            return '';
        }

        $this->link = $node->getAttribute('href');

        $this->setCrawler();

        return $this->link;
    }
}
