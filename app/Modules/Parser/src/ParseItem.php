<?php
declare(strict_types = 1);

namespace App\Modules\Parser\src;

class ParseItem extends BaseParse
{
    protected $data = [];

    protected $parsingMap;

    public function __construct(string $link, iterable $parsingMap)
    {
        parent::__construct($link);
        $this->parsingMap = $parsingMap;
    }

    public function parse(): array
    {
        $this->data = [];

        foreach ($this->parsingMap as $key => $selector) {
            $this->data[$key] = $this->crawler->filter($selector)
                                              ->text();
        }

        return $this->data;
    }
}
