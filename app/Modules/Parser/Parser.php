<?php
declare(strict_types = 1);

namespace App\Modules\Parser;

class Parser
{
    private $link;

    private $itemClass;

    private $categoryClass;

    private $type;

    private $maxObjects;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function loadConfig()
    {
        $config = config('parser');

        $this->link = $config['categories'][$this->type]['link'];
        $this->itemClass = $config['categories'][$this->type]['item_class'];
        $this->categoryClass = $config['categories'][$this->type]['category_class'];

        $this->maxObjects = $config['objects']['max_items'];
    }

    public function run(): void
    {

        /** @var \App\Modules\Parser\src\ParsebleCategory $categoryParser */
        $categoryParser = app()->make($this->categoryClass, ['link' => $this->link]);

        do {
            $itemLinks = $categoryParser->fetchItemList();

            foreach ($itemLinks as $link) {

                /** @var \App\Modules\Parser\src\ParsebleItem $parserItem */
                $parserItem = app()->make($this->itemClass, ['link' => $link]);

                if ($this->maxObjects < $parserItem->getIteratedCount()) {
                    return;
                }

                $parserItem->parse();
            }
        } while (!empty($categoryParser->nextPage()));
    }
}
