<?php
declare(strict_types = 1);

namespace App\Modules\Parser;

use App\Modules\Parser\src\ParsebleItem;
use App\Modules\Parser\src\ParseItem;
use App\Repositories\Bicycle;

class BicycleParser implements ParsebleItem
{
    private static $updated = 0;

    private static $iterated = 0;

    protected $maxUpdated;

    protected $parsingMap = [
        'title' => '#storeapendix h1',
        'price' => '#store_price',
        'type'  => '#for_tab_info table tr[data-info="111"] .value',
        'code'  => '#store_code',
        'year'  => '#for_tab_info table tr[data-info="12"] .value',
    ];

    protected $parser;

    public function __construct(string $link)
    {
        $this->parser = new ParseItem($link, $this->parsingMap);

        $this->maxUpdated = config('parser.update.max_items');
    }

    public function parse(): array
    {
        $data = $this->parser->parse();

        $repository = new Bicycle($data);

        if ($repository->isNew()) {
            $repository->save();
        } else {
            if ($this->canUpdate()) {
                $repository->update();
                self::$updated++;
            }
        }

        self::$iterated++;

        return $data;
    }

    private function canUpdate(): bool
    {
        return $this->maxUpdated > self::$updated;
    }

    public function getIteratedCount(): int
    {
        return self::$iterated;
    }
}
