<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bicycle extends Model
{
    protected $fillable = ['code', 'title', 'price', 'year', 'type'];
}
