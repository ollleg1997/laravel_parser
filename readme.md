## About Laravel Parser

This is a parser on the Laravel framework that collects data from the selected category, saving them to the database.

Data collection:

- the parser enters the link of each listing object and parsits the page of the object;

- data collection is provided in the presence of a page-by-page output;

- already twisted goods are updated

The parser has its own config file with the settings: how many objects to parry in one run, how many objects to update in one run, the number of categories of the parsing for parsing (if to add an additional category, parsing will take into account the record and inside it).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

#
### Configuration
Path: config/parser.php

   - update [array] - Settings for updatable items
        - max_items [int] - Maximum number of elements
    
   - objects [array] - Settings for iterable items
        - max_items [int] - Maximum number of elements
        
   - categories [array]  - List of categories for parsing
        - category [array] - Nickname category
            - link [string] - Link to category page
            - category_class [string] - Class name for category parsing
            - item_class [string] - Class name for item parsing

#
### Running

Command: php artisan parser:run [--category=categoryNickname]

#
### Tool for category parsing

##### *For parsing categories designed \App\Modules\Parser\src\ParseCategory class.*

*_construct* takes 3 parameters:
  - link - Link to the page
  - itemSelector - Selector for category element
  - pageSelector - Selector to the next page (optional)
  
*fetchItemList* - designed to obtain links to category items

*nextPage* - shifts the pointer to the link of the next page of the category. If the link is found, it will return it, otherwise it will return an empty string

     For automatic parsing from the console, you need to create a class
     that implements the interface *\App\Modules\Parser\src\ParsebleCategory*,
     and register it for the category parser.php > categories.category.category_class.
     
     For example, for parcing a bicycle shop, unscrews 
     class *\App\Modules\Parser\src\BicycleCategoryParser*. 
     Now every time we parse pages with bicycles we use 
     it. If the structure of the category has not changed.
     
     
#
### Tool for item parsing

##### *For parsing items designed \App\Modules\Parser\src\ParseItem class.*

*_construct* takes 2 parameters:
  - link - Link to the page
  - parsingMap - An associative array where the value is the selector for the parameter that you want to sparsify.
  
*parse* - Returns an array of parsingMap, the values of which correspond to the found elements on the page

     For automatic parsing from the console, you need to create a class
     that implements the interface *\App\Modules\Parser\src\ParsebleItem*,
     and register it for the category parser.php > categories.category.item_class.
     
     For example, for parcing a bicycle shop, unscrews 
     class *\App\Modules\Parser\src\BicycleParser*. 
     Now every time we parse item with bicycle we use 
     it. If the structure of the category has not changed.

#
### Unit tests
To run the tests, you first need to run node.js server, which is located along the path: tests/Unit/server.js

#
### Installation
    git clone https://ollleg1997@bitbucket.org/ollleg1997/laravel_parser.git
    cd laravel_parser
    composer install
    
    ** cofigure database connection **
    
    php artisan migrate
    php artisan parser:run
